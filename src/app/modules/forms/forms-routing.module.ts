import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { ReactiveComponent } from './pages/reactive/reactive.component';
import { TemplateComponent } from './pages/template/template.component';

const routes: Routes = [
  {
    path: "reactive",
    component: ReactiveComponent
  },
  {
    path: "template",
    component: TemplateComponent
  },
  {
    path: "",
    redirectTo: "template",
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule { }
