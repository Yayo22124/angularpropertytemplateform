import { Component, Inject } from '@angular/core';

import { CustomSnackBarComponent } from 'src/app/modules/forms/components/custom-snack-bar/custom-snack-bar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgForm } from '@angular/forms';
import { Property } from '../../../../core/interfaces/property';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss'],
})
export class TemplateComponent {
  constructor(private _snackbar: MatSnackBar) {}

  property: any = {
    nombre: null,
    descripcion: null,
    precio: null,
    tamano: null,
    numHabitaciones: null,
    numBanos: null,
    numPisos: null,
    antiguedad: null,
    estado: null,
    cp: null,
    localidad: null,
    calle: null,
  };

  guardarPropiedad(form: NgForm) {
    if (form.invalid) {
      this._snackbar.open(`Formulario inválido, verifique los campos.`, 'cerrar');
      return;
    }
    this._snackbar.openFromComponent(CustomSnackBarComponent);
  }
}
