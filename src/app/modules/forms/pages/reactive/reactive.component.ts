import { FormBuilder, Validators } from '@angular/forms';

import { Component } from '@angular/core';
import { CustomSnackBarComponent } from 'src/app/modules/forms/components/custom-snack-bar/custom-snack-bar.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.scss'],
})
export class ReactiveComponent {
  // property: any = {
  //   nombre: null,
  //   descripcion: null,
  //   precio: null,
  //   tamano: null,
  //   numHabitaciones: null,
  //   numBanos: null,
  //   numPisos: null,
  //   antiguedad: null,
  //   estado: null,
  //   cp: null,
  //   localidad: null,
  //   calle: null,
  // };

  // Variable para guardar el formulario
  cpRegex= /^[0-5][1-9]{3}[0-9]$/
  fProperty = this.fb.group({
    nombre: [null, [Validators.required, Validators.minLength(3)]],
    descripcion: [null, [Validators.pattern('[a-zA-Z0-9s]{15,250}')]],
    precio: [null, [Validators.required]],
    tamano: [null, [Validators.required]],
    numHabitaciones: [null, [Validators.required]],
    numBanos: [null, [Validators.required]],
    numPisos: [null],
    antiguedad: [null, [Validators.required]],
    estado: [null, [Validators.required]],
    cp: [null, [Validators.required, Validators.pattern(this.cpRegex)]],
    localidad: [null, [Validators.required]],
    calle: [null, [Validators.required]],
  });
  // Instanciar FormGroup
  constructor(private fb: FormBuilder, private _snackbar: MatSnackBar) {}

  guardarPropiedad() {
    if (this.fProperty.invalid) {
      this._snackbar.open(
        `Formulario inválido, verifique los campos.`,
        'cerrar'
      );
      return;
    }
    this._snackbar.openFromComponent(CustomSnackBarComponent);
  }

  limpiarFormulario(){
    this.fProperty.reset();
  }
}
