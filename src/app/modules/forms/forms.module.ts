import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { CustomSnackBarComponent } from 'src/app/modules/forms/components/custom-snack-bar/custom-snack-bar.component';
import { FormsRoutingModule } from './forms-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormField } from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgModule } from '@angular/core';
import { ReactiveComponent } from './pages/reactive/reactive.component';
import { TemplateComponent } from './pages/template/template.component';

@NgModule({
  declarations: [
    ReactiveComponent,
    TemplateComponent,
    CustomSnackBarComponent
  ],
  imports: [
    CommonModule,
    FormsRoutingModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    FormsModule,
    MatSnackBarModule,
    MatIconModule,
    ReactiveFormsModule
  ]
})
export class FormulariosModule { }
