import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: "",
    redirectTo: "forms",
    pathMatch: 'full'
  },
  {
    path: "forms",
    loadChildren: () => import('./modules/forms/forms.module').then(m => m.FormulariosModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
