export interface Property {
  nombre: string;
  descripcion: string;
  precio: number;
  tamano: number;
  numHabitaciones: number;
  numBanos: number;
  numPisos: number;
  antiguedad: string;
  estado: string;
  cp: number;
  localidad: string;
  calle: string;
}
